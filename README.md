# Get Word With Greatest Letter Frequency

## About

### Author

Ryan E. Anderson

---

### Description

 This function will parse words from a provided input file, perform case-insensitive comparison of letters, and return
 the token that has the greatest frequency of a single letter; if no matches are found, then an empty string will be
 returned.
 
 * Control the encoding.
 * Use an offset to set the zero-based line number where parsing should begin.
 * Pass a variable by reference so that a set of tokens can be accessed outside of the scope of the function.
 * Parse any word that is contracted, hyphenated, and/or possessive.
 * Use a search pattern that will not consider spaces between words while searching for tokens.
 * Use a search pattern that will consider word formations that are either standard or nonstandard.
---

### Version

0.1.0

---

### License

Apache-2.0

---

## Parameters

### $file_path

This is the path of a file containing text that will be parsed to find a word that has the greatest frequency of a 
single letter.

### $encoding

This is the character encoding of the text input. The default value for this parameter is UTF-8.

### $line_offset

This is the zero-based line number where parsing will begin. The default value for this parameter is 0.

### $words

Any token that is parsed from the input text will be stored in this array. The default value for this parameter is null.

## Output

The get_word_with_greatest_letter_frequency function will return a string that, when compared to other tokens, 
represents a word with the greatest frequency of a single letter. Any token that the 
get_word_with_greatest_letter_frequency function can determine will be a word that is or isn't contracted, hyphenated, 
and/or possessive. If the get_word_with_greatest_letter_frequency function cannot find such a word, then an empty string 
will be returned.  

## About the Search Pattern

The get_word_with_greatest_letter_frequency function uses preg_match_all to determine matches based on a regular 
expression. Below is an example of code that demonstrates how preg_match_all is used in the 
get_word_with_greatest_letter_frequency function to search for word matches.

```php
$file_text = "This is text from a file.";

preg_match_all("/[']?[a-zA-Z]+(?:(?<!['])[']?(?:[\-][']?[a-zA-Z]+)*[a-zA-Z]*)*/", $file_text, $output, PREG_PATTERN_ORDER, 0);
```

Note that spaces are not considered in the search pattern, so a user must provide text that is well-formed in order to 
retrieve certain, expected output (Recall the meaning of G.I.G.O.).

## Using get_word_with_greatest_letter_frequency

Below is an example of a script that can be used to evaluate the functionality of 
get_word_with_greatest_letter_frequency.

```php
<?php

require_once("php/func.php/get_word_with_greatest_letter_frequency.func.php");

$input_file = "input/txt/input_f.txt";

$target_word = get_word_with_greatest_letter_frequency($input_file, "UTF-8", 1, $tokens);

$output = [
    "input_file" => $input_file,
    "output" => [
        "tokens" => array_values(array_unique($tokens)),
        "token_count" => sizeof($tokens),
        "target_word" => $target_word
    ]
];

header("Content-Type: application/json");

echo json_encode($output, JSON_PRETTY_PRINT);

exit(0);
```

In this example, the offset is set to one, which starts the parsing at the second line. The offset could be useful in a 
situation where some text, such as a file description, should be ignored. 

## Demo

A demo application is provided for the purpose of evaluating the get_word_with_greatest_letter_frequency function.

### Support

The demo is not supported by legacy contexts. A modern browser must be used to evaluate the functionality of 
get_word_with_greatest_letter_frequency.

### Installation

The demo project contains a configuration.inc.php file, in which the value of the constant for the web root can be 
modified to set the correct path to a directory on a web server. For example, if a virtual directory has not been 
mapped, and the project has been deployed to /some/path/on/a/web/server/, then the value of the constant for the web 
root should be set to that directory path.

Below is the default setting for the web root.

```php
if (!defined("PROJECT_WEB_ROOT"))
    define("PROJECT_WEB_ROOT", "/");
```