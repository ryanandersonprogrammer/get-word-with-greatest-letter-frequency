<?php
/**
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Get Word With Greatest Letter Frequency v0.1.0 Demo Parse Script
 *
 * by Ryan E. Anderson
 *
 * Copyright (C) 2020 Ryan E. Anderson
 */
require_once("inc.php/top.inc.php");

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["path"]) && isset($_POST["encoding"]) && isset($_POST["line-offset"])) {
    $post = array_values($_POST);

    $input_file_path = $_SERVER["DOCUMENT_ROOT"] . $post[0];

    $file_text = "";

    $tokens = [];

    if (!file_exists($input_file_path)) {
        $status = 1;

        $message = "The file does not exist.";
    } else {
        $file_text = file_get_contents($input_file_path);

        try {
            $target = get_word_with_greatest_letter_frequency($input_file_path, $post[1], 1, $tokens);

            $status = 0;

            $message = "The file " . basename($input_file_path) . " was parsed, and the output \"$target\" was produced.";
        } catch (InvalidArgumentException $exception) {
            $status = 1;

            $message = $exception->getMessage();
        }
    }

    $data = [
        "status" => $status,
        "message" => $message,
        "file_contents" => $file_text,
        "tokens" => $tokens
    ];

    header("Content-Type: application/json");

    $output = json_encode($data, JSON_PRETTY_PRINT);

    echo($output);
}

exit(0);