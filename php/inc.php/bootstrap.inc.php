<?php declare(strict_types=1);
/**
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Get Word With Greatest Letter Frequency v0.1.0 Demo Bootstrap Loader
 *
 * by Ryan E. Anderson
 *
 * Copyright (C) 2020 Ryan E. Anderson
 */
if (!defined("PROJECT_APPLICATION_ROOT"))
    exit("The project application root has not been defined. Ensure that the configuration file has been included.");

if (file_exists(PROJECT_APPLICATION_ROOT . "func.php/get_word_with_greatest_letter_frequency.func.php"))
    require_once(PROJECT_APPLICATION_ROOT . "func.php/get_word_with_greatest_letter_frequency.func.php");
else
    exit("The bootstrap loader could not find the function file.");