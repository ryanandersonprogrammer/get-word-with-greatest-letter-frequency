<?php declare(strict_types=1);
/**
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Get Word With Greatest Letter Frequency v0.1.0 Demo Configuration
 *
 * by Ryan E. Anderson
 *
 * Copyright (C) 2020 Ryan E. Anderson
 */
if (!defined("PROJECT_NAME"))
    define("PROJECT_NAME", "Get Word With Greatest Letter Frequency");

if (!defined("PROJECT_WEB_ROOT"))
    define("PROJECT_WEB_ROOT", "/");

if (!defined("PROJECT_ROOT"))
    define("PROJECT_ROOT", $_SERVER["DOCUMENT_ROOT"] . PROJECT_WEB_ROOT);

if (!defined("PROJECT_APPLICATION_ROOT"))
    define("PROJECT_APPLICATION_ROOT", PROJECT_ROOT . "php/");

if (!defined("AUTHORS"))
    define("AUTHORS", ["Ryan E. Anderson"]);

if (!defined("COPYRIGHT_LEFT"))
    define("COPYRIGHT_LEFT", "Copyright"); // This constant can be used to display copyright text that appears to the left of the &copy; entity.

if (!defined("COPYRIGHT_RIGHT"))
    define("COPYRIGHT_RIGHT", "Ryan E. Anderson"); // This constant can be used to display copyright text that appears to the right of the &copy; entity.

if (!defined("COPYRIGHT_START_YEAR"))
    define("COPYRIGHT_START_YEAR", 2020);

if (!defined("COPYRIGHT_END_YEAR"))
    define("COPYRIGHT_END_YEAR", null);