<?php
/**
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Get Word With Greatest Letter Frequency v0.1.0 Demo Sample Script
 *
 * by Ryan E. Anderson
 *
 * Copyright (C) 2020 Ryan E. Anderson
 */
require_once("func.php/get_word_with_greatest_letter_frequency.func.php");

$input_file = "../input/txt/input_f.txt";

$target_word = get_word_with_greatest_letter_frequency($input_file, "UTF-8", 1, $tokens);

$output = [
    "input_file" => $input_file,
    "output" => [
        "tokens" => array_values(array_unique($tokens)),
        "token_count" => sizeof($tokens),
        "target_word" => $target_word
    ]
];

header("Content-Type: application/json");

echo json_encode($output, JSON_PRETTY_PRINT);

exit(0);