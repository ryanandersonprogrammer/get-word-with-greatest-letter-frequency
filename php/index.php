<!DOCTYPE html>
<!--
Copyright 2020 Ryan E. Anderson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

<!--
    Get Word With Greatest Letter Frequency v0.1.0 Demo Dynamic Index

    by Ryan E. Anderson

    Copyright (C) 2020 Ryan E. Anderson
-->
<?php require_once("inc.php/top.inc.php"); ?>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <link rel="apple-touch-icon" sizes="180x180"
          href="../media/png/Get-Word-With-Greatest-Letter-Frequency-Icon-180.png">
    <!--[if IE]>
    <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico"/>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="../css/get-word-with-greatest-letter-frequency-demo-style.css">

    <script src="../js/jquery-3.4.1.min.js"></script>

    <title><?php echo(PROJECT_NAME); ?> - Demo</title>
</head>
<body>
<div id="wrapper">
    <header>
        <h1 class="text"><?php echo(PROJECT_NAME); ?> - Demo</h1>
    </header>
    <main>
        <article>
            <h2 class="text">Get Word with Greatest Letter Frequency</h2>
            <div class="clear-fix"></div>
            <div class="author">
                by <?php echo(implode(", ", AUTHORS)); ?> on
                <time datetime="2020-12-31" title="December 31st, 2020">12/31/2020</time>
            </div>
            <section>
                <h3 class="text">Code</h3>
                <p class="text text-paragraph">
                    Below is the implementation of the get_word_with_greatest_letter_frequency function.
                </p>
                <div class="clear-fix"></div>
                <pre class="formatted-text formatted-text-no-wrap code gray">
                <code><?php
                    $file_contents =
                        file_get_contents("func.php/get_word_with_greatest_letter_frequency.func.php");

                    $contents_with_entities = htmlentities($file_contents);

                    echo($contents_with_entities);
                    ?>
                </code>
            </pre>
            </section>
            <section>
                <h3 class="text">List of Input Files</h3>
                <p id="list-options" class="text text-paragraph">Display as <a href="#list-options">ul</a>|<a
                            href="#list-options">dl</a>|<a
                            href="#list-options">ol</a>.</p>
                <p class="text text-paragraph">
                    Click on an input file.
                </p>
                <h3 class="text">File Contents</h3>
                <p class="text text-paragraph">
                    Below is the container for the text content of a selected input file. The first line of each input
                    file contains a description, which is skipped during parsing because the line offset for each call
                    to get_word_with_greatest_letter_frequency is set to one.
                </p>
                <div class="clear-fix"></div>
                <pre id="contents" class="formatted-text formatted-text-wrap gray"></pre>
                <h3 class="text">Tokens</h3>
                <h4 class="text">CSV</h4>
                <p id="tokens" class="text text-paragraph">
                    No input files have been parsed. Click on an input file to generate tokens.
                </p>
                <h4 class="text">Count</h4>
                <p id="token-count" class="text text-paragraph">
                    No input files have been parsed. Click on an input file to generate tokens.
                </p>
                <h3 class="text">Output</h3>
                <p id="response" class="text text-paragraph">
                    No input files have been parsed. Click on an input file to generate output.
                </p>
                <div class="clear-fix"></div>
                <div id="error" class="hidden">
                    <h3 class="text">Error</h3>
                    <p class="text text-paragraph">
                        There are no errors to report.
                    </p>
                    <div class="clear-fix"></div>
                </div>
            </section>
        </article>
    </main>
    <footer>
        <p class="text text-paragraph fine-print">
            <small></small>
        </p>
    </footer>
    <div class="clear-fix"></div>
</div>
<script>
    const setFinePrint = (window, selector, copyrightTextLeft, copyrightTextRight, copyrightStartYear = null, copyrightEndYear
        = null) => {
        if (!(window instanceof Window))
            throw "The value of window must be an instance of Window.";

        const windowDocument = window.document;
        const element = windowDocument.querySelector(selector);

        if (!Number.isInteger(copyrightStartYear))
            copyrightStartYear = new Date().getFullYear();

        let copyrightText = `${copyrightTextLeft} \u00A9 ${copyrightStartYear}`;

        if (Number.isInteger(copyrightEndYear))
            copyrightText += `\u2013${copyrightEndYear}`;

        copyrightText += ` ${copyrightTextRight}`;

        element.appendChild(windowDocument.createTextNode(copyrightText));
    };

    const listWebFiles = (window, webFiles, webFileIdentifierKey, localFilePathKey, webFilePathKey, webFileNameKey,
                          webFileDescriptionKey, adjacentElement, inputListIdentifier, listType = 1,
                          descriptionListClasses = [], orderedListClasses = [], unorderedListClasses = []) => {
        if (!(window instanceof Window))
            throw "The value of window must be an instance of Window.";

        if (!Array.isArray(webFiles) || webFiles.length === 0 || webFiles.filter(webFile => !(typeof webFile ===
            "object" && Object.prototype.hasOwnProperty.call(webFile, webFileIdentifierKey)
            && Object.prototype.hasOwnProperty.call(webFile, localFilePathKey)
            && Object.prototype.hasOwnProperty.call(webFile, webFilePathKey)
            && Object.prototype.hasOwnProperty.call(webFile, webFileNameKey)
            && Object.prototype.hasOwnProperty.call(webFile, webFileDescriptionKey)
            && Number.isInteger(webFile[webFileIdentifierKey]) && typeof webFile[localFilePathKey] === "string" &&
            typeof webFile[webFilePathKey] === "string" && typeof webFile[webFileNameKey] === "string" && typeof
                webFile[webFileDescriptionKey] === "string")).length > 0)
            throw `The list of web files must be an array of objects that have the following properties: \
${webFileIdentifierKey} as an integer, ${localFilePathKey} as a string, ${webFilePathKey} as a string, \
${webFileNameKey} as a string, and ${webFileDescriptionKey} as a string.`;

        if (typeof inputListIdentifier !== "string")
            throw "The value for the identifier of the input list must be a string.";

        if (!Object.prototype.isPrototypeOf.call(window.HTMLElement.prototype, adjacentElement))
            throw "The adjacent element must be an HTML element that is specified by either a known or unknown tag " +
            "name.";

        const validateClassList = (classList) => {
            if (!Array.isArray(classList) || classList.filter(c => typeof c !== "string").length > 0)
                throw "Each class list must be an array of strings";
        };

        validateClassList(descriptionListClasses);
        validateClassList(orderedListClasses);
        validateClassList(unorderedListClasses);

        const windowDocument = window.document;

        let classList;
        let list;
        let isDescriptionList;

        isDescriptionList = false;

        switch (listType) {
            case 1:
                list = windowDocument.createElement("ul");
                classList = unorderedListClasses;
                break;
            case 2:
                list = windowDocument.createElement("ol");
                classList = orderedListClasses;

                webFiles.sort((a, b) => {
                    if (a[webFileIdentifierKey] < b[webFileIdentifierKey])
                        return -1;

                    if (a[webFileIdentifierKey] > b[webFileIdentifierKey])
                        return 1;

                    return 0;
                });

                break;
            case 3:
                list = windowDocument.createElement("dl");
                classList = descriptionListClasses;
                isDescriptionList = true;
                break;
            default:
                throw "The list type that was provided is not supported.";
        }

        list.setAttribute("id", inputListIdentifier);

        const classListLength = classList.length;

        for (let i = 0; i < classListLength; i++)
            list.classList.add(classList[i]);

        webFiles.forEach(file => {
            const listItemLink = windowDocument.createElement("a");

            listItemLink.setAttribute("href", `${file[webFilePathKey]}${file[webFileNameKey]}`);
            listItemLink.appendChild(windowDocument.createTextNode(file[webFileNameKey]));

            if (isDescriptionList) {
                const descriptionTitleFileName = windowDocument.createElement("dt");
                const descriptionTermFileDescription = windowDocument.createElement("dd");

                descriptionTitleFileName.appendChild(listItemLink);

                descriptionTermFileDescription.appendChild(windowDocument.createTextNode(file[webFileDescriptionKey]));

                list.appendChild(descriptionTitleFileName);
                list.appendChild(descriptionTermFileDescription);
            } else {
                const listItem = windowDocument.createElement("li");

                listItem.appendChild(listItemLink);

                list.appendChild(listItem);
            }
        });

        if (adjacentElement.insertAdjacentElement("afterend", list) === null)
            throw "Insertion of the adjacent element failed.";

        return list;
    };

    const toggleContent = (window, element, classToRemove, classToAdd, contentSelector, content) => {
        if (!(window instanceof Window))
            throw "The value of window must be an instance of Window.";

        if (!Object.prototype.isPrototypeOf.call(window.HTMLElement.prototype, element))
            throw "The element must be an HTML element that is specified by either a known or unknown tag name.";

        if (typeof classToRemove !== "string")
            throw "The value of the class to remove must be a string.";

        if (typeof classToAdd !== "string")
            throw "The value of the class to add must be a string.";

        const contentElement = element.querySelector(contentSelector);

        if (contentElement === null)
            throw "The content element could not be found.";

        const elementClassList = element.classList;

        if (elementClassList.contains(classToRemove)) {
            elementClassList.remove(classToRemove);
            elementClassList.add(classToAdd);

            contentElement.removeChild(contentElement.firstChild);
            contentElement.appendChild(content);
        }
    };

    $(() => {
        const finePrintRequest = {
            type: "GET",
            url: "get_fine_print.php",
            dateType: "json",
            cache: false
        };
        const webFileRequest = {
            type: "GET",
            url: "get_web_files.php",
            dataType: "json",
            cache: false
        };
        const finePrintResponse = $.ajax(finePrintRequest);
        const webFileResponse = $.ajax(webFileRequest);
        const secondSection = document.querySelectorAll("section")[1];
        const adjacentElement = secondSection.querySelectorAll("p")[1];
        const errorElement = $("#error");
        const errorElementNative = errorElement.get(0);

        let files;
        let list;

        finePrintResponse.then((data) => {
            const finePrint = Object.prototype.hasOwnProperty.call(data, "fine_print")
            && Object.prototype.hasOwnProperty.call(data["fine_print"], "copyright_left")
            && Object.prototype.hasOwnProperty.call(data["fine_print"], "copyright_right")
            && Object.prototype.hasOwnProperty.call(data["fine_print"], "copyright_start_year")
            && Object.prototype.hasOwnProperty.call(data["fine_print"], "copyright_end_year") ? data["fine_print"] : {
                "copyright_left": "Copyright",
                "copyright_right": "Ryan E. Anderson",
                "copyright_start_year": 2020,
                "copyright_end_year": null
            };

            setFinePrint(window, "p.fine-print > small", finePrint["copyright_left"], finePrint["copyright_right"],
                finePrint["copyright_start_year"], finePrint["copyright_end_year"]);
        });

        webFileResponse.then((data) => {
            files = Object.prototype.hasOwnProperty.call(data, "web_files") ? data["web_files"] : [];

            $("#list-options > a").on("click", (event) => {
                let listType;

                const linkText = $(event.currentTarget).text();

                toggleContent(window, errorElementNative, "visible", "hidden", "p", document.createTextNode("There " +
                    "are no errors to report."));

                switch (linkText) {
                    case "ul":
                        listType = 1;
                        break;
                    case "ol":
                        listType = 2;
                        break;
                    case "dl":
                        listType = 3;
                        break;
                    default:
                        throw "A valid list option could not be found.";
                }

                if (typeof list !== "undefined")
                    list.remove();

                list = listWebFiles(window, files, "web_file_identifier", "local_file_path", "web_file_path",
                    "web_file_name", "web_file_description", adjacentElement, "input-list", listType, ["clear-fix",
                        "list"], ["clear-fix", "list", "list-indent"], ["clear-fix", "list", "list-indent"]);

                $("ul#input-list > li > a, ol#input-list > li > a, dl#input-list > dt > a").on("click", (event) => {
                    event.preventDefault();

                    const anchor = $(event.currentTarget);

                    const currentLink = anchor.attr("href");

                    const previousLink = $("a.current");

                    if (previousLink.length > 0 && previousLink.hasClass("current"))
                        previousLink.removeClass("current");

                    if (!anchor.hasClass("current"))
                        anchor.addClass("current");

                    const request = {
                        type: "POST",
                        url: "parse.php",
                        data: {
                            path: currentLink,
                            encoding: "UTF-8",
                            "line-offset": 1 // This offset is used to ignore a description of the contents, which is on the first line of each input file.
                        },
                        dataType: "json",
                        cache: false
                    };

                    const response = $.ajax(request);

                    response.then((data) => {
                        const contents = Object.prototype.hasOwnProperty.call(data, "file_contents") ?
                            data["file_contents"] : "None.";
                        const status = Object.prototype.hasOwnProperty.call(data, "status") ? data["status"] : 1;
                        const message = Object.prototype.hasOwnProperty.call(data, "message") ? data["message"] :
                            "None.";

                        let errorElementText;
                        let classToRemove;
                        let classToAdd;
                        let responseMessage;
                        let tokens;
                        let tokenText;
                        let tokenCount;

                        $("#contents").text(contents);

                        if (Object.prototype.hasOwnProperty.call(data, "tokens") && (tokenCount = (tokens =
                            data["tokens"]).length) > 0)
                            tokenText = tokens.join(", ");
                        else {
                            tokenText = "None.";

                            tokenCount = 0;
                        }

                        $("#tokens").text(tokenText);
                        $("#token-count").text(tokenCount);

                        if (status === 1) {
                            classToAdd = "visible";
                            classToRemove = "hidden";

                            responseMessage = "None.";

                            errorElementText = message;
                        } else {
                            classToAdd = "hidden";
                            classToRemove = "visible";

                            responseMessage = message;

                            errorElementText = "There are no errors to report.";
                        }

                        toggleContent(window, errorElementNative, classToRemove, classToAdd, "p",
                            document.createTextNode(errorElementText));

                        $("#response").text(responseMessage);
                    });
                });
            });

            $("#list-options > a:first-of-type").click();
        }).catch((e) => {
            if (errorElement.hasClass("hidden")) {
                errorElement.removeClass("hidden");
                errorElement.addClass("visible");

                errorElement.find("p").text(e);
            }
        });
    });
</script>
</body>
</html>