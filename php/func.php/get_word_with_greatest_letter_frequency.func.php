<?php declare(strict_types=1);
/**
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * get_word_with_greatest_letter_frequency
 *
 * This function will parse words from a provided input file, perform case-insensitive comparison of letters, and return
 * the token that has the greatest frequency of a single letter; if no matches are found, then an empty string will be
 * returned. This function will parse any word that is or isn't contracted, hyphenated, and/or possessive. This function
 * will not consider spaces between words while searching for tokens.
 *
 * @param string $file_path This is the path of a file containing text that will be parsed to find a word that has the
 * greatest frequency of a single letter.
 * @param string $encoding This is the character encoding of the text input. The default value for this parameter is
 * UTF-8.
 * @param int $line_offset This is the zero-based line number where parsing will begin. The default value for this
 * parameter is 0.
 * @param array|null $words Any tokens that are parsed from the input text will be stored in this array. The default
 * value for this parameter is null.
 * @return string Any word that is or isn't contracted, hyphenated, and/or possessive will be parsed from the input text
 * and returned from this function. If no such token can be determined, then an empty string will be returned.
 *
 * @author Ryan E. Anderson
 * @license https://www.apache.org/licenses/LICENSE-2.0 Apache-2.0
 * @copyright Copyright (C) 2020 Ryan E. Anderson
 * @version 0.1.0
 */
function get_word_with_greatest_letter_frequency(string $file_path, string $encoding = "UTF-8", int $line_offset = 0, array &$words = null)
{
    if (!file_exists($file_path))
        throw new InvalidArgumentException("The provided file path is invalid.");

    $file = file($file_path);

    if ($line_offset < 0 || $line_offset + 1 > sizeof($file))
        throw new InvalidArgumentException("The provided line offset is invalid.");

    $target_word = "";

    $offset = 0;

    for ($i = 0; $i < $line_offset; $i++)
        $offset += mb_strlen($file[$i], $encoding); // Count the length of each line before the offset.

    $file_text = file_get_contents($file_path, false, null, $offset);

    preg_match_all("/[']?[a-zA-Z]+(?:(?<!['])[']?(?:[\-][']?[a-zA-Z]+)*[a-zA-Z]*)*/", $file_text, $output, PREG_PATTERN_ORDER, 0);

    $output_first_match_set = $output[0];

    $output_length = sizeof($output_first_match_set);

    if ($output_length > 0) {
        $words = $output_first_match_set;

        $count = 1; // This variable stores the greatest count of all words (This will be at least one because of the regular expression and the condition that qualifies the output length.).

        $target_word = $words[0];

        foreach ($words as $word) {
            $word_length = mb_strlen($word, $encoding);

            $count_per_word = 1; // This variable stores the greatest count of the current word (This will be at least one because of the regular expression and the condition that qualifies the output length.).

            $temporary_word = str_replace(["'", "-"], "", $word, $removed); // Remove apostrophes and hyphens.

            do {
                $letter = $temporary_word[0]; // Get the next letter.

                $temporary_word = str_ireplace($letter, "", $temporary_word, $count_letter); // Remove all occurrences of the current letter.

                $removed += $count_letter;

                if ($count_letter > $count_per_word)
                    $count_per_word = $count_letter;
            } while ($removed < $word_length);

            if ($count_per_word > $count) {
                $count = $count_per_word;

                $target_word = $word;
            }
        }
    }

    return $target_word;
}