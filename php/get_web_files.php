<?php
/**
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Get Word With Greatest Letter Frequency v0.1.0 Demo Get Web Files Script
 *
 * by Ryan E. Anderson
 *
 * Copyright (C) 2020 Ryan E. Anderson
 */
require_once("inc.php/top.inc.php");

$input_file_path = "input/txt";

$local_directory_path = PROJECT_ROOT . $input_file_path;

$web_files = new DirectoryIterator($local_directory_path);

$web_file_objects = [];

$i = 0;

foreach ($web_files as $web_file) {
    if (!$web_file->isDot() && $web_file->isFile()) {
        $first_line = file($web_file->getPathname())[0];

        $web_file_description = preg_split("/[\r\n\t\f\v]*/", $first_line);

        $web_file_objects[] = [
            "web_file_identifier" => ++$i,
            "web_file_name" => $web_file->getBasename(),
            "local_file_path" => "$local_directory_path/",
            "web_file_path" => PROJECT_WEB_ROOT . "$input_file_path/",
            "web_file_description" => implode("", $web_file_description)
        ];
    }
}

header("Content-Type: application/json");

echo(json_encode(["web_files" => $web_file_objects], JSON_PRETTY_PRINT));